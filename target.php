<?php
/**
 * Created by PhpStorm.
 * User: orcungumus
 * Date: 2.05.15
 * Time: 22:52
 */

include_once('includes/error_log.php');
include_once ('includes/config.php');
include_once('struct/Gamer.php');
//sleep(3);
session_start();
$person = Gamer::get($conn,$_SESSION["bilkentid"],$_SESSION["password"]) ;
if(isset($person) && $person)
    $target = $person->get_target($conn);
?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Assasin Bilkent</title>

    <!-- Bootstrap Core CSS - Uses Bootswatch Flatly Theme: http://bootswatch.com/flatly/ -->
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/freelancer.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>


<body id="page-top" class="index">

<!-- Navigation -->
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header page-scroll">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#page-top">Bilkent Board Gamers</a>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li class="hidden">
                    <a href="#page-top"></a>
                </li>

                <li class="page-scroll active">
                    <a  href="target.php">Target</a>
                </li>

                <li class="page-scroll">
                    <a href="profile.php">Profile</a>
                </li>
                <li class="page-scroll">
                    <a onclick="flush();" href="" >Log out</a>
                </li>


            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-fluid -->
</nav>

<section id="portfolio" class="margin-top">
    <div class="container">
        <?php
        if(isset( $target)  &&  $target) {
            echo "
<div class=\"row\"><br>
    <div class=\"text-center\">
        <h2><small>Your target is </small><br>
        $target->fullname
        </h2>

        <hr class=\"star-primary\">
    </div>
</div>
<div class=\"row\">
    <div class=\"col-sm-6\">
        <div class=\"row\">
            <img width=\"400px\" height=\"400px\" src=\"upload/$target->bilkentid.png\" class=\"img-circle center\" >
        </div>
        <div class=\"text-center\">
            <p ><h3><small>Bilkent ID: </small>$target->bilkentid</h3></p>
        </div>
    </div>
    <div class=\"col-sm-6\">
         <p ><span id=\"clock\"></span></p>
        <p ><h3 class=\"info\"><small>Dormitory: </small></h3>$target->dormitory</p>
        <p ><h3 class=\"info\"><small>Department: </small></h3>$target->department</p>
         <div class=\"row\">

            <div class=\"col-xs-4\">
                <button href=\"#confirm-remove\" data-toggle=\"modal\" type=\"button\" class=\"btn btn-info\">I killed it</button>
            </div>

            <div id=\"getting-started\"></div>
        </div>
    </div>";
        }

        else {
            echo " <h3 class='center-block center text-center'>You do not have any target for now</h3>
            ";}?>
    </div>

</section>


<!-- Footer -->
<footer class="text-center">
    <div class="footer-above">
        <div class="container">
            <div class="row">

                <div class="footer-col col-md-6">
                    <h3>Around the Web</h3>
                    <ul class="list-inline">
                        <li>
                            <a href="https://www.facebook.com/groups/bilkentBG/?fref=ts" class="btn-social btn-outline"><i class="fa fa-fw fa-facebook"></i></a>
                        </li>

                    </ul>
                </div>
                <div class="footer-col col-md-6">
                    <h3>About Bilkent Board Gamers</h3>
                    <p>bilboard is brand new club of Bilkent join us via  <a href="https://www.facebook.com/groups/bilkentBG/?fref=ts">facebook</a>.</p>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-below">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    Copyright &copy; bilboard 2015
                </div>
            </div>
        </div>
    </div>
</footer>

<div id="confirm-remove"  class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Confirm Abandonment
                </h4>
            </div>
            <div class="modal-body">
                <p>Your datas will remove from sistem and you can not rejoin in this tourniment during 24 hours </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">No, I want a board game! - Cancel</button>
                <button   onclick="deactivate()" type="button" class="btn btn-primary"  data-dismiss="modal">No more water! - Delete my account</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- jQuery -->
<script src="js/jquery.js"></script>
<script src="js/jquery.countdown.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="js/bootstrap.js"></script>
<script src="js/contact_me.js"></script>

</body>

</html>





