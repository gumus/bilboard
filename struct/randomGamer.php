<?php

include_once('../includes/error_log.php');
include_once('../includes/config.php');
include_once('Gamer.php');
include_once('User.php');

session_start();


if(isset($_SESSION) && isset($_SESSION['user'])) {
    $user = $_SESSION['user'];

    if ($user->get_authenticated())
        echo "";
    else {
        unset($_SESSION['user']);
        die();
    }
}
else {
    header("Location: adminLogin.php");
    die();
}

if(isset($_POST['filter'])){
    if(isset($_POST["bilkentid"])){
        if(isset($_POST["delete"])){
            Gamer::delete($conn, $_POST["bilkentid"]);
        }
        else if (isset($_POST["pass"])){
            Gamer::pass($conn, $_POST["bilkentid"]);
        }
    }
    $person = Gamer::control($conn);
    show_all_data($person);
    echo '<br><br><br><br><div class="row">';
    echo '<div class="col-md-3"> All application: ' . Gamer::number_of_person($conn) . '</div>';
    echo '<div class="col-md-3"> Activated application: ' . Gamer::number_of_activated($conn) . '</div>';
    echo '<div class="col-md-3"> Activated and deleted application: ' . Gamer::number_of_deleted($conn) . '</div>';
    echo '<div class="col-md-3"> Activated and confirmed application: ' . Gamer::number_of_confirmed($conn) . '</div></div>';
}
else if (isset($_POST['target'])){

    if(isset($_POST["fromGamer"])){
        if(isset($_POST["toGamer"])){
            Gamer::set_target($conn, $_POST["fromGamer"],$_POST["toGamer"]);
        }
    }

    $person = Gamer::random_from($conn);
    $targets = Gamer::possible_targets($conn);
    if($person)
        show_image($person);
    else
        echo "<div class=\"row center text-center\">
                <h2>Finitto<br><small>Everybody has a target</small></h2>
                </div>";

    if(count($targets)>0)
        show_targets($targets,$person);
    else
        echo "<div class=\"margin-top-lg row center text-center\">
                <h2>Finitto<br><small>Everybody is a target</small></h2>
                </div>";
}

function show_targets($targets,$person){
    echo "<br><br><br><div lass=\"row\">";
    $i = 0;

    foreach($targets as $gamer) {

        if ($i % 4 == 0)
            echo "
            <div  onclick='setTarget($person->bilkentid,$gamer->bilkentid)' class=\"col-md-3 text-center center \">
        ";
        echo "
            <img  width=\"200px\" height=\"200px\" src=\"upload/$gamer->bilkentid.png\" class=\"img-circle center\" ></div>
        ";
        if ($i % 4 == 3)
            echo "</div>";
    $i++;
    }
    echo "</div>";

}

function show_image($person){
    echo "
    <div lass=\"row\">
        <input  type='hidden' value=\"$person->bilkentid\" id=\"bilkentid\">
        <img width=\"400px\" height=\"400px\" src=\"upload/$person->bilkentid.png\" class=\"img-circle center\" >
    </div>";
}

function show_all_data($person)
{
    if ($person) {
        echo "
<div class=\"row\">
    <div class=\"col-md-6 text-center center \">
    <div lass=\"row\">
    <input  type='hidden' value=\"$person->bilkentid\" id=\"bilkentid\">
        <img width=\"400px\" height=\"400px\" src=\"upload/$person->bilkentid.png\" class=\"img-circle center\" >
    </div>
    <div lass=\"row\">
        <br><br><br>
        <div class=\"col-md-6\">
            <button onclick=\"dlte()\" type=\"button\" class=\"btn btn-warning btn-lg\">Delete</button>
        </div>
        <div class=\"col-md-6\">
            <button onclick='pass()' type=\"button\" class=\"btn btn-success btn-lg\">Pass</button>
        </div>
    </div>
    </div>
    <div class=\"col-md-6\">
        <h2>$person->fullname</h2>
        <p ><h3><small>Bilkent ID: </small>$person->bilkentid</h3></p>
        <p  ><h3 class=\"info\"><small>Bilkent Mail: </small></h3>$person->email</p>
        <p ><h3 class=\"info\"><small>Dormitory: </small></h3>$person->dormitory</p>
        <p  ><h3 class=\"info\"><small>Apply Date: </small></h3>$person->sdate</p>
        <p  ><h3 class=\"info\"><small>Facebook URL: </small></h3><a href=\"$person->facebook\"></a>$person->facebook</p>
        <p  ><h3 class=\"info\"><small>Phone: </small></h3>$person->bilkentid</p>
        <p ><h3 class=\"info\"><small>IP: </small></h3>$person->ip</p>
        <p  ><h3 class=\"info\"><small>Message:  </small></h3>$person->message</p>
    </div>
</div>
";
    } else {
        echo "
<div class=\"row center text-center\">
<h2>Finitto<br><small>You finished to control all application</small></h2>
</div>
";
    }

}

?>