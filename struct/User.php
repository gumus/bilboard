<?php
/**
 * Created by PhpStorm.
 * User: orcungumus
 * Date: 11/12/14
 * Time: 13:43
 */



class User{

    public $username;
    public $password;
    private $authenticated = false;

    public $errors = array();

    function __construct($user){
        $this->username = $user["username"];
        $this->password = $user["password"];
    }

    public function get_authenticated(){
        return $this->authenticated;
    }


    public static function find_by_username($username,$conn){

        $return_value = false;

        $sql = "SELECT username, password
            FROM admin
            WHERE username = \"" . $username . "\";";


        $result = $conn->query($sql);

        $row = $result->fetch_assoc();
        $return_value = new User($row);
        return $return_value;
    }

    public function authenticate($mysqli){
        $return_value = false;


        if (($user = User::find_by_username($this->username,$mysqli)) && ($user->password ==  $this->password)){
            $this->authenticated = true;
            $return_value = true;
        }
        return $return_value;
    }
}
/* DEBUG
echo "<pre>";
print_r($user);
echo "</pre>";
*/