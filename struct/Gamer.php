<?php
/**
 * Created by PhpStorm.
 * User: orcungumus
 * Date: 1.05.15
 * Time: 14:08
 */

class Gamer
{
    protected $info = array();

    public static $vars = array('bilkentid', 'password', 'sdate', 'fullname', 'dormitory', 'email', 'department', 'phone', 'facebook', 'ip', 'message', 'enable', 'confirm');
    public $error = "";

    public function __set($name, $value = '')
    {
        $this->$name = $value;
    }

    public function &__get($name)
    {
        return $this->info[$name];
    }

    public function __construct(array $info = null)
    {

        foreach ($info as $key => $value) {
            if (in_array($key, self::$vars))
                $this->info[$key] = $value;
        }
        return true;
    }

    public function insert($conn)
    {

        if ($this->readyUpload()) {
            $this->delete_if_exist($conn);

            $query = 'INSERT INTO gamer (bilkentid,password , sdate, fullname, dormitory, email, department, phone, facebook, ip, message, enable) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);';
            $stmt = $conn->prepare($query);
            $enable = 0;
            $date = date("Y-m-d H:i:s");
            $stmt->bind_param("iisssssssssi", $this->info["bilkentid"], $this->info["password"], $date, $this->info["fullname"], $this->info["dormitory"], $this->info["email"], $this->info["department"], $this->info["phone"], $this->info["facebook"], $_SERVER['REMOTE_ADDR'], $this->info["message"], $enable);
            if ($stmt->execute()) {
                return true;
            } else {
                $this->error = "Insertation";
                return false;
            }
        } else {
            $this->error = "ReadyUpload";
            return false;
        }
    }

    public static function get($conn, $id, $password)
    {
        if (isset($id)) {

            $query = 'SELECT * FROM gamer WHERE bilkentid = ' . $id . ' AND password = '.$password.';';
           // echo $query;
            $result = $conn->query($query);

            $row = $result->fetch_assoc();

            return new Gamer($row);
        }

        return false;

    }

    private function delete_if_exist($conn)
    {

        if (!isset($this->info["bilkentid"]))
            return false;

        $src = "upload/" . $this->info["bilkentid"] . ".png";

        if (file_exists($src)) {
            unlink($src);
        }

    }

    private function readyUpload()
    {
        print_r($this->info);
        if (isset($this->info["bilkentid"]) && isset($this->info["fullname"]) && isset($this->info["dormitory"]) && isset($this->info["department"]) && isset($this->info["email"]) && (isset($this->info["phone"]) || isset($this->info["facebook"])))
            return true;
        else {
            return false;
        }
    }

    public function get_target($conn){

        $query = 'SELECT gamer.* FROM target INNER JOIN gamer ON gamer.bilkentid = target.toGamer WHERE bilkentid = '.$this->bilkentid.' AND DAY(d) = DAY(CURDATE()) ;';
        $result = $conn->query($query);
        $row = $result->fetch_assoc();
        if($result->num_rows>0)
            return new Gamer($row);
        else return false;

    }



    public function assasination_success($conn,$id){

        $sql = "UPDATE target SET sucess = sucess + 100 WHERE fromGamer = $this->bilkentid WHERE id = $id;";
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        return ($stmt->affected_rows > 0);

    }

    public function assasination_fail($conn,$id){

        $sql = "UPDATE target SET sucess = sucess - 100 WHERE fromGamer = $this->bilkentid WHERE id = $id;";
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        return ($stmt->affected_rows > 0);

    }

    public static function instance($bilkentid){
        return new Gamer(array("bilkentid"=>$bilkentid));
    }

    public static function set_target($conn,$fromid,$targetid){

        $query = 'INSERT INTO target (fromGamer,toGamer,d,success) VALUES (?, ?, ?,?);';
        $stmt = $conn->prepare($query);
        $date = date("Y-m-d H:i:s"); $success = 0;
        $stmt->bind_param("iisi", $fromid, $targetid, $date, $success);
        if ($stmt->execute()) {
            return true;
        } else {
            return false;
        }

    }

    public static function possible_targets($conn){

        $gamers = array();
        $query = 'SELECT * FROM gamer WHERE gamer.confirm > 0 AND gamer.enable > 0 AND gamer.alive > 0 AND gamer.bilkentid NOT IN (SELECT toGamer FROM target WHERE DAY(d) = DAY(CURDATE()));';
        $result = $conn->query($query);
        $numResults = $result->num_rows;
        for ($i = 0; $i < $numResults; $i++) {
            $row = $result->fetch_assoc();
            $gamers[] = new Gamer($row);

       //     print_r($gamers);

        }
        return $gamers;
    }

    public static function deactivate($conn, $bilkentid, $password){

        $sql = "UPDATE gamer SET enable = -100 WHERE password = $password AND bilkentid = $bilkentid ;";
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        return ($stmt->affected_rows > 0);

    }

    public static function random_from($conn){

        $query = 'SELECT * FROM gamer WHERE gamer.confirm > 0 AND gamer.enable > 0 AND gamer.alive > 0 AND gamer.bilkentid NOT IN (SELECT fromGamer FROM target WHERE DAY(d) = DAY(CURDATE())) LIMIT 1;';
        $result = $conn->query($query);
        $row = $result->fetch_assoc();
        $numResults = $result->num_rows;
        if($numResults > 0)
            return new Gamer($row);
        return false;

    }

    public static function control($conn){

        $query = 'SELECT * FROM gamer WHERE confirm = 0 AND enable > 0 LIMIT 1;';
        $result = $conn->query($query);
        $row = $result->fetch_assoc();
        if($result->num_rows>0)
        return new Gamer($row);
        else return false;
    }

    public static function number_of_person($conn){

        $query = 'SELECT count(*) AS number_of FROM gamer;';
        $result = $conn->query($query);
        $row = $result->fetch_assoc();
        return $row["number_of"];

    }
    public static function number_of_activated($conn){

        $query = 'SELECT count(*) AS number_of FROM gamer WHERE enable>0;';
        $result = $conn->query($query);
        $row = $result->fetch_assoc();
        return $row["number_of"];


    }
    public static function number_of_confirmed($conn){


        $query = 'SELECT count(*) AS number_of FROM gamer WHERE enable>0 AND confirm >0;';
        $result = $conn->query($query);
        $row = $result->fetch_assoc();
        return $row["number_of"];

    }

    public static function number_of_deleted($conn){


        $query = 'SELECT count(*) AS number_of FROM gamer WHERE enable>0 AND confirm <0;';
        $result = $conn->query($query);
        $row = $result->fetch_assoc();
        return $row["number_of"];

    }


    public static function delete($conn, $bilkentid){

        $sql = "UPDATE gamer SET confirm = confirm - 100 WHERE bilkentid = $bilkentid ;";
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        return ($stmt->affected_rows > 0);

    }
    public static function pass($conn, $bilkentid){

        $sql = "UPDATE gamer SET confirm = confirm + 100 WHERE bilkentid = $bilkentid ;";

        // Prepare statement
        $stmt = $conn->prepare($sql);

        $stmt->execute();
        return ($stmt->affected_rows > 0);

    }

    public static function activate($conn, $bilkentid, $password){
        $sql = "UPDATE gamer SET enable = enable + 1 WHERE password = $password AND bilkentid = $bilkentid ;";

        // Prepare statement
        $stmt = $conn->prepare($sql);
        //echo $sql;
        // execute the query
        $stmt->execute();
        return ($stmt->affected_rows > 0);
    }


}