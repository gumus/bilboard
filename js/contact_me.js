// Required for drag and drop file access

var formdata = false; var avatarData; var avatarSrc; var avatarFile;

jQuery.event.props.push('dataTransfer');
var d = new Date();
d.setSeconds(0);
d.setMinutes(0);
d.setHours(23);
//d.setDate(d.getDate());
$('#clock').countdown(d, function(event) {

       $(this).html(event.strftime('<h4>%H hours %M Minutes %S Seconds </h4>'));
     });

$(document).ready(function(){
    if ($('#password').length) {
        $('#password').keypress(function (e) {
            if (e.keyCode == 13)
                activate();
        });
    }
});

$("body").on('drop', function(event) {
    // Or else the browser will open the file
    event.preventDefault();

    var file = event.dataTransfer.files[0];
    loaded(file);
    reader = new FileReader();
    reader.readAsDataURL(file);

});

$("body").on("dragover", function(event) {

    // Do something to UI to make page look droppable

    // Required for drop to work
    return false;
});

$("body").on("dragleave", function(event) {

    // Remove UI change

});


function placeNewAvatar(data) {
    $('#profile-avatar').cropper('destroy');

    $("#profile-avatar").attr("src", data);


    $('#profile-avatar').cropper({
        aspectRatio: 1,
        crop: function(data){
            var json = [
                '{"x":' + data.x,
                '"y":' + data.y,
                '"height":' + data.height,
                '"width":' + data.width,
                '"rotate":' + data.rotate + '}'
            ].join();

            avatarData = json;
        }
    });


}


$("#uploader").on('change', function(event) {
    var file = event.target.files[0];
    avatarSrc = file;
    loaded(file);
    reader = new FileReader();
    reader.readAsDataURL(file);

    avatarFile = file;

});
function loaded(file) {
    avatarSrc = file.name;
    if (!file.type.match('image.*')) {
        $("#alerts").html('<div class="alert alert-warning" role="alert">File type: <b>Fault!</b> Not an image, are you joking?</div>');
    }
    else if (file.size > 1000000) {

        $("#alerts").html('<div class="alert alert-warning" role="alert">File Size: <b>Fault!</b> This image is too big</div>');
    }
    else {
        var fileTracker = new FileReader;
        fileTracker.onload = function (e) {
            placeNewAvatar(e.target.result);
        }
        fileTracker.readAsDataURL(file);
        avatarFile = file;
        $("#alerts").html('<div class="alert alert-info" role="alert">File Size: <b>OK!</b>  Clearness will be cheak later</div>');
    }

}
function check_full(){
    if(!avatarFile){
        $("#danger").html("You need an image");
        return false;
    }
    else if($("#email").val().indexOf("bilkent.edu.tr") < 0)
    {
        $("#danger").html("You did not entered a valid bilkent mail adress");
        return false;
    }
    else if(!$("#idnumber").val() || !$("#depertment").val() || !$("#dormitory").val() || (!$("#phone").val() && !$("#facebook").val()) || !$("#name").val()) {
        $("#danger").html("You did not fullfill - you must give all your data except mobile phone");
        return false;
    }
    else{
        return true;
    }

}
function upload() {

    var formdata = new FormData();

    if(check_full()){

        if($("#idnumber").val())
            formdata.append("bilkentid", $("#idnumber").val());
        if($("#depertment").val())
            formdata.append("department", $("#depertment").val());
        if($("#email").val())
            formdata.append("email", $("#email").val());
        if($("#dormitory").val())
            formdata.append("dormitory", $("#dormitory").val());
        if($("#phone").val())
            formdata.append("phone", $("#phone").val());
        if($("#facebook").val())
            formdata.append("facebook", $("#facebook").val());
        if($("#name").val())
            formdata.append("fullname", $("#name").val());
        if($("#message").val())
            formdata.append("message", $("#message").val());


        $.ajax({
            url: "struct/upload.php",
            type: "POST",
            data: formdata,
            processData: false,
            contentType: false
        }).done(function( msg ) {
            if(msg.indexOf("valid") >= 0 ) {


                var fd_image = new FormData();

                fd_image.append("avatar_file", avatarFile);
                fd_image.append("avatar_src", avatarFile.name);
                fd_image.append("avatar_data", avatarData);
                fd_image.append("number", $("#idnumber").val());

                $.ajax({
                    url: 'crop.php',
                    type: 'post',
                    data: fd_image,
                    dataType: 'json',
                    processData: false,
                    contentType: false

                });
                //window.location = "profile.php";
            }
            else {
                $("#danger").html("Something has gone wrong");
            }
        });

    } else {
    }
}

function deactivate(){
    var fd_activate = new FormData();
    fd_activate.append("password", $('#password').val());
    fd_activate.append("bilkentid", $('#bilkentid').val());
    fd_activate.append("activate", "d");

    $.ajax({
        url: 'struct/activate.php',
        type: 'post',
        data: fd_activate,
        processData: false,
        contentType: false

    }).done(function( msg ) {
        if (msg.indexOf("valid") >= 0) {
            window.location = "profile.php";
        }
        window.location = "profile.php";

    });
}


function activate(){
    var fd_activate = new FormData();
    fd_activate.append("password", $('#password').val());
    fd_activate.append("bilkentid", $('#bilkentid').val());
    fd_activate.append("activate", "a");

    $.ajax({
        url: 'struct/activate.php',
        type: 'post',
        data: fd_activate,
        processData: false,
        contentType: false

    }).done(function( msg ) {
        if (msg.indexOf("valid") >= 0) {
            window.location = "profile.php";
        }
        window.location = "profile.php";

    });
}

function flush(){
    $.ajax({
        url: 'struct/flush.php'

    }).done(function( msg ) {
        window.location = "profile.php";
    });
}