/**
 * Created by orcungumus on 3.05.15.
 */
$( document ).ready(function() {
    newPerson();

});

function newPerson(){

    var fd  = new FormData();
    fd.append("target", 1);
    $.ajax({
        url: 'struct/randomGamer.php',
        type: 'post',
        data: fd,
        processData: false,
        contentType: false

    }).done(function( msg ) {
        $("#person").html(msg);
    });

}

function setTarget(from,to){
    var fd  = new FormData();
    fd.append("target", 1);
    fd.append("fromGamer", from);
    fd.append("toGamer", to);

    $.ajax({
        url: 'struct/randomGamer.php',
        type: 'post',
        data: fd,
        processData: false,
        contentType: false

    }).done(function( msg ) {
        $("#person").html(msg);
    });
}