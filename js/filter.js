/**
 * Created by orcungumus on 2.05.15.
 */
$( document ).ready(function() {
    var fd  = new FormData();
    fd.append("filter", 1);
    $.ajax({
        url: 'struct/randomGamer.php',
        type: 'post',
        data: fd,
        processData: false,
        contentType: false

    }).done(function( msg ) {
        $("#person").html(msg);
    });

});

function pass(){
    var fd_activate = new FormData();
    fd_activate.append("bilkentid", $('#bilkentid').val());
    fd_activate.append("pass", "pass");
    fd_activate.append("filter", 1);

    $.ajax({
        url: 'struct/randomGamer.php',
        type: 'post',
        data: fd_activate,
        processData: false,
        contentType: false

    }).done(function( msg ) {
        $("#person").html(msg);
    });
}

function dlte(){
    var fd_activate = new FormData();
    fd_activate.append("bilkentid", $('#bilkentid').val());
    fd_activate.append("delete", "delete");
    fd_activate.append("filter", 1);

    $.ajax({
        url: 'struct/randomGamer.php',
        type: 'post',
        data: fd_activate,
        processData: false,
        contentType: false
    }).done(function( msg ) {
        $("#person").html(msg);
    });
}