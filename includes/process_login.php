<?php
include_once("config.php");
include_once("error_log.php");

include_once("../struct/User.php");

session_start();

if(isset($_GET["logout"])){
    $_SESSION['user'] = null;
    header('Location: ../index.php');
}
else {

    $user = new User($_POST['user']);


    if ($user->authenticate($conn)) {
        $_SESSION['user'] = $user;
        header("Location: ../filter.php");
    } else {
        if (isset($_SESSION['login_failure'])) {
            $login_failure = $_SESSION['login_failure'];
            $login_failure[] = "username_not_exist";
            $_SESSION['login_failure'] = $login_failure;

            header("Location: ../filter.php");
        } else {
            $login_failure = array();
            $login_failure[] = "username_not_exist";
            $_SESSION['login_failure'] = $login_failure;
            header("Location: ../filter.php");
        }

    }
}

