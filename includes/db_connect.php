<?php
include_once 'config.php';   // As functions.php is not included
@ $mysqli = new mysqli(HOST, USER, PASSWORD, DATABASE);

class database_functions{
    //SELECT users.* FROM users WHERE username = ?
    public static function bind_results($stmt){
        $params = array();
        $results = array();

        $meta = $stmt->result_metadata();

        while( $field = $meta->fetch_field() ) {
            $params[] = &$row[$field->name];
        }
        call_user_func_array(array($stmt, 'bind_result'), $params);// Call the $stmt->bindresult() method with 2 arguments

        while( $stmt->fetch() ) {
            $x = array();
            foreach( $row as $key => $value ) {
                $x[$key] = $value;
            }
            $results[] = (object) $x;
        }

        return $results;
    }
}

?>