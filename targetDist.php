<?php
include_once('includes/error_log.php');
include_once ('includes/config.php');
include_once('struct/Gamer.php');
include_once('struct/User.php');
//sleep(3);

session_start();
$aut = false;
if(isset($_SESSION) && isset($_SESSION['user'])) {
    $user = $_SESSION['user'];

    if ($user->get_authenticated())
        print_r($user);
    else {
        unset($_SESSION['user']);
        die();
    }
}
else {
    header("Location: adminLogin.php");
    die();
}


?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Assasin Bilkent</title>

    <!-- Bootstrap Core CSS - Uses Bootswatch Flatly Theme: http://bootswatch.com/flatly/ -->
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/freelancer.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<!-- Navigation -->
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header page-scroll">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#page-top">Bilkent Board Gamers</a>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li class="active">
                    <a href="targetDist.php">Target</a>
                </li>
                <li >
                    <a href="filter.php">Confirm</a>
                </li>
                <li >
                    <a href="includes/process_login.php?logout"><i class="fa fa-fw fa-power-off"></i>Admin Log Out</a>
                </li>
            </ul>
        </div>

        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-fluid -->
</nav>
<section id="portfolio" class="margin-top">
    <div class="container" id="person">

    </div>
</section>
<!-- Footer -->

<footer class="text-center">
    <div class="footer-above">
        <div class="container">
            <div class="row">

                <div class="footer-col col-md-6">
                    <h3>Around the Web</h3>
                    <ul class="list-inline">
                        <li>
                            <a href="https://www.facebook.com/groups/bilkentBG/?fref=ts" class="btn-social btn-outline"><i class="fa fa-fw fa-facebook"></i></a>
                        </li>

                    </ul>
                </div>
                <div class="footer-col col-md-6">
                    <h3>About Bilkent Board Gamers</h3>
                    <p>bilboard is brand new club of Bilkent join us via  <a href="https://www.facebook.com/groups/bilkentBG/?fref=ts">facebook</a>.</p>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-below">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    Copyright &copy; bilboard 2015
                </div>
            </div>
        </div>
    </div>
</footer>

<!-- jQuery -->
<script src="js/jquery.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="js/bootstrap.js"></script>
<script src="js/targetDist.js"></script>


</body>

</html>


