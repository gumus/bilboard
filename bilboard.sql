/*
 Navicat Premium Data Transfer

 Source Server         : local
 Source Server Type    : MySQL
 Source Server Version : 50542
 Source Host           : localhost
 Source Database       : bilboard

 Target Server Type    : MySQL
 Target Server Version : 50542
 File Encoding         : utf-8

 Date: 05/02/2015 01:46:37 AM
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `admin`
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin` (
  `username` char(20) NOT NULL,
  `password` char(10) NOT NULL,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `admin`
-- ----------------------------
BEGIN;
INSERT INTO `admin` VALUES ('root', 'root');
COMMIT;

-- ----------------------------
--  Table structure for `gamer`
-- ----------------------------
DROP TABLE IF EXISTS `gamer`;
CREATE TABLE `gamer` (
  `bilkentid` int(11) NOT NULL,
  `password` int(11) DEFAULT NULL,
  `sdate` datetime DEFAULT NULL,
  `fullname` char(50) NOT NULL,
  `dormitory` char(50) NOT NULL,
  `email` char(50) NOT NULL,
  `department` char(50) NOT NULL,
  `phone` char(20) DEFAULT NULL,
  `facebook` char(100) DEFAULT NULL,
  `ip` char(10) NOT NULL,
  `message` text,
  `enable` int(11) DEFAULT NULL,
  `confirm` int(11) DEFAULT NULL,
  PRIMARY KEY (`bilkentid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `gamer`
-- ----------------------------
BEGIN;
INSERT INTO `gamer` VALUES ('1', null, '2015-04-30 22:32:17', 'qwd', 'dqw', 'dqw', 'dqw', 'asc', 'dqw', '::1', null, '0', '0'), ('12', null, '2015-04-30 20:57:18', '12', '21', '12', 'undefined', 'undefined', 'undefined', '', null, '0', '0'), ('123', '123', '2015-05-01 19:00:07', 'qwd', 'sca', 'bilkent.edu.tr', 'ac', 'cas', 'cas', '::1', null, '18', '0'), ('3123', null, '2015-04-30 21:27:54', '', '', '', '', '', '', '', null, '0', '0'), ('21312', null, '2015-05-01 14:49:47', 'ds', 'acs', 'bilkent.edu.tra', 'cd', 'acs', 'acs', '::1', null, '0', '0'), ('12213123', null, '2015-04-30 21:27:03', 'Adc', '', '', '', '', '', '', null, '0', '0'), ('21201288', '3060', '2015-05-01 21:15:37', 'Ahmet Baglan', '43', 'orcun.gumus@ug.bilkent.edu.tr', 'CS', '5342314323', 'www.facebook.com/ahmentbaglan', '::1', 'Ahmet ben sevgiler', '7', '0');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
