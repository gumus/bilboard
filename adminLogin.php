<?php
include_once("struct/User.php");

session_start();

if(isset($_SESSION) && isset($_SESSION['user'])) {
    $user = $_SESSION['user'];

    if($user->get_authenticated())
        header("Location: filter.php");
    else
        unset($_SESSION['user']);
}
?>

<form action="includes/process_login.php" method="post" name="login_form">
    <div class="form-group">
        <label >Username</label>
        <input type="username" class="form-control" name="user[username]" placeholder="Enter username">
    </div>
    <div class="form-group">
        <label ">Password</label>
        <input type="password" class="form-control" name="user[password]" placeholder="Password">
    </div>
    <button type="submit" class="btn btn-default">Log in</button>
</form>
</div>