<?php
include_once('includes/error_log.php');
include_once ('includes/config.php');
include_once('struct/Gamer.php');


session_start();
$person = Gamer::get($conn,$_SESSION["bilkentid"],$_SESSION["password"]) ;
if($person  && $person->enable > 0)
    header("Location: profile.php");

?>



<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">


    <title>Assasin Bilkent</title>

    <!-- Bootstrap Core CSS - Uses Bootswatch Flatly Theme: http://bootswatch.com/flatly/ -->
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/freelancer.css" rel="stylesheet">
    <link href="css/cropper.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">



    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body id="page-top" class="index">

    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#page-top">Bilkent Board Gamers</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
                    <li class="page-scroll">
                        <a  href="#about">Rules</a>
                    </li>
                    <li class="page-scroll">
                        <a href="#contact">Join</a>
                    </li>
                    <li class="page-scroll">
                        <a href="profile.php">My Profile</a>
                    </li>

                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>

    <!-- Header -->
    <header>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <img class="img-responsive" src="img/watergun.png" alt="">
                    <div class="intro-text">
                        <span class="name">Bilkent Assassin</span>
                        <hr class="star-light">
                        <span class="skills">hunt bilkent students with a water gun, be the last one who survives and win a board game!</span>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <!-- Portfolio Grid Section -->
    <section id="portfolio">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2>How does it Work?</h2>
                    <hr class="star-primary">
                </div>
            </div>
            <div class="portfolio-content">
                <div class="row">
                    <div class="col-sm-4 portfolio-item">
                        <img width="200px" src="img/letter.png" class="img-circle center" alt="">
                        <p class="portfolio-text">Join to become an assassin. During six beautiful summer days you will be given a target at every 02:00 , and 21 hours to eliminate your target. You wiil be able to eliminate your target before 23:00. </p>
                    </div>


                    <div class="col-sm-4 portfolio-item">
                        <img width="200px" src="img/assasin.png" class="img-circle center"  alt="">
                        <p class="portfolio-text">Your weapon will be basic water gun which will be distributed, but you have the power to turn it into the mightiest weapon in the world. Don't forget, you are also someone else's target. Sometimes the hunter might become the hunted. Every participant will gain a basic water gun for nothing at start. </p>
                    </div>

                    <div class="col-sm-4 portfolio-item">
                        <img width="200px" src="img/winner.png" class="img-circle center" alt="">
                        <p class="portfolio-text">When you are only one person who has not eliminated, you are the winner. First prize is a new board game named <a href="https://eksisozluk.com/dixit--2825078">DIXIT</a> which worth 90 Turkish Liras. Partnership with other Bilkent Clubs is in process, possibly there will be suprize prizes  </p>
                    </div>
                </div>
                <div class="row"" >

                    <iframe class="center center-block" width="840" height="630"
                            src="https://www.youtube.com/embed/UNhj7OpJtjc">
                    </iframe>
                    <p class="center center-block text-center">Stanford students are shairng their experiances :)</p>
                </div>
            </div>
        </div>
    </section>

    <!-- About Section -->
    <section class="success" id="about">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2>Rules</h2>
                    <hr class="star-light">
                </div>
            </div>
            <div class="row">
                <div class="rules col-md-6">
                    <ul>
                        <li>You should have bilkent mail to join this event</li>
                        <li>The assassination must not be witnessed by any other than you and your target. If someone see you during your assasination you will be eliminated. But 2 second after assasination event you can be seen freely</li>
                        <li>The target is eliminated when water hits him/her. Purses, backpacks and other accesories can't protect you from your demise, if water hits them, he/she was dead. </li>
                        <li>Once you kill your target, you will be assigned his/her target at next 02:00.</li>
                        <li>An assassin must be smart and subtle, you can't use physical force on your target.</li>
                        <li>You can use your own waterguns and waterbaloons</li>
                        <li>You can gain small prizes like card games by recording assaninations or select your killer image correctly from profile page. </li>
                    </ul>
                </div>
                <div class="rules col-md-6">
                    <ul>
                        <li>You must inform us your successful assasination via this web site before midnight. If you fail to kill your target within 21 hours, you will be terminated.  </li>
                        <li>You will learn your target from your profile with your password and bilkentid. </li>
                        <li>When you eliminated we ask from you that inform us before 00:00 from your profile to avoid false pretenses </li>
                        <li>If your target shoots you before you assassinate him/her, you are neutralized for 60 minutes. During this time you can't kill anyone.</li>
                        <li>You can't take other assassins' guns while they are holding.</li>
                        <li>You can share your ideas and complaints to our organization team via facebook.</li>
                        <li>We will not share your phone number or facebook with other assasins, we are taking them in order to communicate with you as organization team.</li>
                        <li>Lying about anything in the game means you will be eliminated directly.</li>
                    </ul>
                </div>

            </div>
        </div>
    </section>

    <!-- Contact Section -->
    <section id="contact">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2>Join Tournament</h2>
                    <hr class="star-primary">

                        <?php

                        $number = Gamer::number_of_person($conn);
                        $number = 20 - $number;
                        if($number < 0)
                            echo'
                            <small>. There are no more free tickets. You can purchase tickets from organization team. Have a nice game. Find us on facebook.</small>
                </div>
            </div>

                            ';
                        else {
                            echo '

                    <small>Do not use Turkish characters. Last possible ' . $number . ' free ticket, take your place quick. Ticket number can increase related deleted accounts by organisation team. Follow us.</small>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2">
                    <!-- To configure the contact form email address, go to mail/contact_me.php and update the email address in the PHP file on line 19. -->
                    <!-- The form should work on most web servers, but if the form is not working you may need to configure your web server differently. -->
                    <div class="row control-group">
                        <div class="form-group col-xs-12 floating-label-form-group controls">
                            <label>Name</label>
                            <input type="text" class="form-control" placeholder="Melis Basaran - name" id="name" required data-validation-required-message="Please enter your name.">
                            <p class="help-block text-danger"></p>
                        </div>
                    </div>
                    <div class="row control-group">
                        <div class="form-group col-xs-12 floating-label-form-group controls">
                            <label>Bilkent Email Address</label>
                            <input type="email" class="form-control" placeholder="name.surname@ug.bilkent.edu.tr - bilkent mail" id="email" required data-validation-required-message="Please enter your email address.">
                            <p class="help-block text-danger"></p>
                        </div>
                    </div>
                    <div class="row control-group">
                        <div class="form-group col-xs-12 floating-label-form-group controls">
                            <label>Bilkent ID Number</label>
                            <input type="email" class="form-control" placeholder="XXXXXXXX - bilkent id" id="idnumber" required data-validation-required-message="Please enter your email address.">
                            <p class="help-block text-danger"></p>
                        </div>
                    </div>
                    <div class="row control-group">
                        <div class="form-group col-xs-12 floating-label-form-group controls">
                            <label>Department</label>
                            <input type="text" class="form-control" placeholder="CS - department" id="department" required data-validation-required-message="Please enter your email address.">
                            <p class="help-block text-danger"></p>
                        </div>
                    </div>
                    <div class="row control-group">
                        <div class="form-group col-xs-12 floating-label-form-group controls">
                            <label>Dormitory - /N for Residantel</label>
                            <input type="text" class="form-control" placeholder="78 - dormitory" id="dormitory" required data-validation-required-message="Please enter your email address.">
                            <p class="help-block text-danger"></p>
                        </div>
                    </div>
                    <div class="row control-group">
                        <div class="form-group col-xs-12 floating-label-form-group controls">
                            <label>facebook link /N for NULL</label>
                            <input type="text" class="form-control" placeholder="www.facebook.com/mlsbsrn - facebook URL" id="facebook" required data-validation-required-message="Please enter your email address.">
                            <p class="help-block text-danger"></p>
                        </div>
                    </div>
                    <div class="row control-group">
                        <div class="form-group col-xs-12 floating-label-form-group controls">
                            <label>Phone /N for NULL</label>
                            <input type="text" class="form-control" placeholder="507XXXXXXX - phone" id="phone" required data-validation-required-message="Please enter your email address.">
                            <p class="help-block text-danger"></p>
                        </div>
                    </div>
                    <div class="row control-group">
                        <div class="form-group col-xs-12 floating-label-form-group controls">
                            <div class="row">
                                <br>
                                <div class="col-sm-6">
                                    <img width="250px" height="250px" src="img/256.jpg" id="profile-avatar" class="img-circle center" alt="Image for Profile">

                                </div>
                                <div class="col-sm-6">
                                    <span class="btn btn-default btn-file">
                                        Browse Picture or Drag Drop<input id="uploader" type="file">
                                    </span>
                                    <p>
                                        Choose your best picture. Your face must be clearly seen and centered. There should be nobody except you. Your account can be deleted because of blurred pictures. Maximum 1 MB.
                                        <div id="alerts"></div>
                                    </p>
                                </div>
                                <br>
                            </div>
                        </div>
                    </div>

                    <div class="row control-group">
                        <div class="form-group col-xs-12 floating-label-form-group controls">
                            <label>Message</label>
                            <textarea rows="5" class="form-control" placeholder="Personal message to organization team" id="message" required data-validation-required-message="Please enter a message."></textarea>
                            <p id="danger" class="help-block text-danger"></p>
                        </div>
                    </div>

                    <div id="success"></div>
                    <div class="row">
                        <div class="form-group col-xs-12">
                            <br>
                            <a href="#confirm" data-toggle="modal" class="btn btn-success btn-lg center">Apply</a>
                        </div>
                    </div>

                </div>
            ';
                        }
                        ?>


            </div>
        </div>
    </section>

    <!-- Footer -->
    <footer class="text-center">
        <div class="footer-above">
            <div class="container">
                <div class="row">

                    <div class="footer-col col-md-6">
                        <h3>Around the Web</h3>
                        <ul class="list-inline">
                            <li>
                                <a href="https://www.facebook.com/groups/bilkentBG/?fref=ts" class="btn-social btn-outline"><i class="fa fa-fw fa-facebook"></i></a>
                            </li>

                        </ul>
                    </div>
                    <div class="footer-col col-md-6">
                        <h3>About Bilkent Board Gamers</h3>
                        <p>Bilboard is brand new club of Bilkent. Join us via  <a href="https://www.facebook.com/groups/bilkentBG/?fref=ts">facebook</a>.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-below">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        Copyright &copy; bilboard 2015
                    </div>
                </div>
            </div>
        </div>
    </footer>



    <!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
    <div class="scroll-top page-scroll visible-xs visible-sm">
        <a class="btn btn-primary" href="#page-top">
            <i class="fa fa-chevron-up"></i>
        </a>
    </div>

    <div id="confirm"  class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Confirm Attendance</h4>
                </div>
                <div class="modal-body">
                    <p>These data will be controlled by our organizators, please </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Where am I?</button>
                    <button   onclick="upload()" type="button" class="btn btn-primary"  data-dismiss="modal">I know what I do!</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
    <script src="js/resample.js"></script>
    <script src="js/classie.js"></script>
    <script src="js/cropper.js"></script>
    <script src="js/cbpAnimatedHeader.js"></script>
    <script src="js/freelancer.js"></script>
    <script src="js/contact_me.js"></script>

</body>

</html>
